# good_sellers

This is a Rails 4.2.6 app.

## Documentation

This README describes the purpose of this repository and how to set up a development environment. Other sources of documentation are as follows:

* UI and API designs are in `doc/`

## Prerequisites

This project requires:

* Ruby 2.3.0
* MySQL must be installed and accepting connections

Optional but helpful software:
* Homebrew (OS X Package Manager)
* RVM (Ruby Version Manager)

If you need help setting up a Ruby development environment, check out this [Rails OS X Setup Guide](https://mattbrictson.com/rails-osx-setup-guide).

## Getting started

### bin/setup

Run the `bin/setup` script. This script will:

* Check you have the required Ruby version
* Install gems using Bundler
* Create a local copy of `database.yml`
* Create, migrate, and seed the database

### Setting up the app manually

* Install required software
* Run `bundle install`
* Run `cp config/database.yml.example config/database.yml`
* Run `rake db:create`
* Run `rake db:migrate`
* Run `rails s`

### Run it!

1. Run `rake test` to make sure everything works.
2. Run `rails s` to start the Rails app.